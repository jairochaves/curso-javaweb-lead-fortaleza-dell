<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Info Filmes</title>
</head>
<body>
	<h2>Informações de Filmes</h2>
	<form action="/CatalogoDeFilmes/processa-info-filme" method="get">
		<label for="">Nome: </label>
		<input id="nome" name="nome" type="text" >
		<br><br>
		<label for="">Gênero: </label>
		<input id="genero" name="genero" type="text" >
		<br><br>
		<label for="">Ano: </label>
		<input id="ano" name="ano" type="text" >
		<br><br>
		<button type="submit"> Enviar </button>
	</form>
</body>
</html>