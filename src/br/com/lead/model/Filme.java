package br.com.lead.model;

public class Filme {
	private String nome;
	private String genero;
	private Integer ano;
	
	public Filme(String nome, String genero, Integer ano) {
		this.nome= nome;
		this.genero=genero;
		this.ano=ano;
	}
	public Integer getAno() {
		return ano;
	}
	public String getGenero() {
		return genero;
	}
	public String getNome() {
		return nome;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
