package br.com.lead.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.lead.model.Filme;
@WebServlet("/processa-info-filme")
public class InfoFilmes extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nome = req.getParameter("nome");
		String genero = req.getParameter("genero");
		String ano = req.getParameter("ano");
		Filme myfilme = new Filme(nome, genero, Integer.parseInt(ano));
		
		
		ArrayList<Filme> filmes=new ArrayList<Filme>();
		filmes.add(myfilme);
		
		ArrayList<Filme> listaFiltrada=filmes.stream()
				.filter(
					filme-> true)
				.collect(Collectors.toCollection(ArrayList::new));
		req.setAttribute("listFiltrada", listaFiltrada);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/lista-filmes.jsp");
		dispatcher.forward(req, resp);
	}
}