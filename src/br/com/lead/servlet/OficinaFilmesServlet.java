package br.com.lead.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.lead.model.Filme;
@WebServlet("/oficina-filme")
public class OficinaFilmesServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String anoString = req.getParameter("Ano");
		
		Filme coringa = new Filme("Coringa", "Drama", 2019);
		Filme matrix = new Filme("Matrix", "Ação", 1991);
		Filme forestGump = new Filme("Forest Gump", "Drama", 1994);
		
		ArrayList<Filme> filmes=new ArrayList<Filme>();
		filmes.add(coringa);
		filmes.add(matrix);
		filmes.add(forestGump);
		
		Integer ano =Integer.parseInt(anoString);
		resp.setContentType("text/HTML");
		PrintWriter out = resp.getWriter();
	
		out.println("<H2>Lista de Filmes, utilizando Servlets</H2>");
		out.println("<ol>");
		filmes.stream().filter(filme-> filme.getAno()>=ano).forEach(filme->{
			out.println("<li>");
			out.println(String.format("Nome: %s", filme.getNome()));
			out.println(String.format("Genero: %s", filme.getGenero()));
			out.println(String.format("Ano: %s", filme.getAno()));
			out.println("</li>");
			
		});
		out.println("</ol>");
		out.close();
	}
}